
import { handleActions } from 'redux-actions';
import { selectCharacter, clearSelected } from '../actions/characters';

const charactersList = [
    {
        "name": "Luke Skywalker",
        "url": "https://swapi.co/api/people/1/"
    },
    {
        "name": "Darth Vader",
        "url": "https://swapi.co/api/people/4/"
    },
    {
        "name": "Obi-wan Kenobi",
        "url": "https://swapi.co/api/people/unknown/"
    },
    {
        "name": "R2-D2",
        "url": "https://swapi.co/api/people/3/"
    }
]

const initialState = {
    characters: {
        list: charactersList,
        selected: {},
        errors: [],
    },
    films: {
        filmsLinks: [],
        filmsDetails: [],
        errors: [],
    },
}

const characters = handleActions({
    [selectCharacter](state, action) {
        return {
            ...state,
            selected: action.payload,
        };
    },
    [clearSelected](state, action) {
        return {
            ...state,
            selected: {},
        }
    }
}, initialState.characters);

export default initialState;