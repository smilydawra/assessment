import { createAction } from 'redux-actions';

export const selectCharacter = createAction('selectCharacter', async (character) => {
    return await character;
});

export const clearSelected = createAction('clearSelected');