import { Box } from '@chakra-ui/react';
import React from 'react';

const Footer = () => {
    return (
        <Box w="100%" h="50px" bgColor="#CBD5E0">
            Footer
        </Box>
    )
}

export default Footer;