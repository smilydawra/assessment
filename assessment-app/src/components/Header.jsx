import { Box } from '@chakra-ui/react';
import React from 'react';

const Header = () => {
    return (
        <Box as="nav" w="100%" h="30px" bgColor="#CBD5E0">
            Header
        </Box>
    )
}

export default Header;