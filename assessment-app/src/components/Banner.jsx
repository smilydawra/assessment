import { Box, Image } from '@chakra-ui/react';
import React from 'react';

const Banner = () => {
    return (
        <Box mt={8}>
            <Image w="100%" src="https://images-na.ssl-images-amazon.com/images/I/61%2BTFTVHK9L._AC_SY355_.jpg" alt="Segun Adebayo" />
        </Box>
    )
}

export default Banner;
